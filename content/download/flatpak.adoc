+++
title = "Install using Flatpak"
distro = "Flatpak"
summary = "Install instructions for KiCad on Linux using Flatpak"
iconhtml = "<div class='fl-flathub'></div>"
weight = 10
+++
:dist: Flatpak

== Stable Release
Version: image:https://img.shields.io/flathub/v/org.kicad.KiCad?color=bright[]

To install KiCad using the Software app:

image:https://flathub.org/assets/badges/flathub-badge-en.png[width=240px, link='https://flathub.org/repo/appstream/org.kicad.KiCad.flatpakref']

Alternatively, you can use the Terminal to install using `flatpak` command:

[source,bash]
flatpak install --from https://flathub.org/repo/appstream/org.kicad.KiCad.flatpakref

== Flathub source

You can find the build manifest to build from source link:https://github.com/flathub/org.kicad.KiCad[here].
